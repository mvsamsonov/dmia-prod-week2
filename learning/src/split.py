import argparse
from pathlib import Path

import pandas as pd
import yaml
from sklearn.model_selection import train_test_split

with open("params.yaml", "r") as f:
    params = yaml.safe_load(f)["split"]


def split(config_path):
    if config_path is None:
        config_path = "params.yaml"

    with open(config_path, "r") as f:
        all_params = yaml.safe_load(f)
    params = all_params["split"]
    base_params = all_params["base"]

    input_path = Path(base_params["raw-folder"]) / base_params["raw-filename"]

    output = Path(base_params["processed-folder"])
    output.mkdir(parents=True, exist_ok=True)
    output_train_path = output / base_params["train-filename"]
    output_test_path = output / base_params["test-filename"]

    df = pd.read_csv(input_path).dropna().drop_duplicates()

    train, test = train_test_split(
        df, test_size=params["test-size"], random_state=base_params["random-state"],
    )

    train.to_parquet(output_train_path, index=False)
    test.to_parquet(output_test_path, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path to params.yaml file")
    args = parser.parse_args()

    split(args.config)
