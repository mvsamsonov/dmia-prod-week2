import argparse
import json
import pickle
from pathlib import Path

import numpy as np
import pandas as pd
import yaml
from sklearn.metrics import mean_squared_log_error


def evaluate(config_path):
    if config_path is None:
        config_path = "params.yaml"

    with open(config_path, "r") as f:
        all_params = yaml.safe_load(f)
    params = all_params["evaluate"]
    base_params = all_params["base"]

    model_path = base_params["model-path"]
    test_data_path = (
        Path(base_params["processed-folder"]) / base_params["test-filename"]
    )

    with open(model_path, "rb") as f:
        pipe = pickle.load(f)

    test = pd.read_parquet(test_data_path)
    X_test = test["Password"]
    y_test = test["Times"]

    pred_log = pipe.predict(X_test)
    pred = np.exp(pred_log).clip(min=1)

    rmsle = np.sqrt(mean_squared_log_error(y_test.values, pred.values))
    print(f"RMSLE: {rmsle}")

    Path(params["test-metrics-path"]).parent.mkdir(parents=True, exist_ok=True)
    with open(params["test-metrics-path"], "w") as f:
        json.dump({"rmsle": rmsle}, f)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path to params.yaml file")
    args = parser.parse_args()

    evaluate(args.config)
