import argparse
import json
import pickle
from pathlib import Path

import numpy as np
import pandas as pd
import yaml
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import Ridge
from sklearn.pipeline import Pipeline

with open("params.yaml", "r") as f:
    params = yaml.safe_load(f)["train"]


def train(config_path):
    if config_path is None:
        config_path = "params.yaml"

    with open(config_path, "r") as f:
        all_params = yaml.safe_load(f)
    params = all_params["train"]
    base_params = all_params["base"]

    output_model_path = base_params["model-path"]
    Path(output_model_path).parent.mkdir(parents=True, exist_ok=True)

    input_data_path = (
        Path(base_params["processed-folder"]) / base_params["train-filename"]
    )

    train = pd.read_parquet(input_data_path)
    X = train["Password"]
    y = np.log(train["Times"])

    print(f"Params: {params}")

    pipe = Pipeline(
        [
            (
                "tfidf",
                TfidfVectorizer(
                    analyzer="char",
                    ngram_range=params["ngram-range"],
                    max_features=params["max-features"],
                ),
            ),
            (
                "ridge",
                Ridge(alpha=params["alpha"], random_state=base_params["random-state"],),
            ),
        ]
    )

    print("Training model...")
    pipe.fit(X, y)

    with open(output_model_path, "wb") as f:
        pickle.dump(pipe, f)

    print("Model saved to pickle")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path to params.yaml file")
    args = parser.parse_args()

    train(args.config)
