import os
import pickle

import numpy as np
from flask import Flask, jsonify, render_template, request
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired

app = Flask(__name__)

SECRET_KEY = os.urandom(32)
app.config["SECRET_KEY"] = SECRET_KEY

with open("../models/model.pickle", "rb") as file:
    pipe = pickle.load(file)


@app.route("/ping")
def ping():
    return "OK"


@app.route("/api/v1/predict", methods=["POST"])
def predict():
    passwords = request.get_json()["passwords"]

    pred_log = pipe.predict(passwords)
    pred = np.exp(pred_log).clip(min=1).tolist()

    return jsonify(pred)


class PasswordForm(FlaskForm):
    password = StringField("Password", validators=[DataRequired()])


@app.route("/demo", methods=["GET", "POST"])
def demo():
    form = PasswordForm()

    if form.validate_on_submit():
        password = form.password.data
        pred_log = pipe.predict([password])[0]
        pred = np.exp(pred_log).clip(min=1)

        return render_template("demo.html", form=form, result=pred)
    return render_template("demo.html", form=form, result=None)
